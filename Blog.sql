-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for blog
CREATE DATABASE IF NOT EXISTS `blog` /*!40100 DEFAULT CHARACTER SET armscii8 COLLATE armscii8_bin */;
USE `blog`;

-- Dumping structure for table blog.articles
DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` mediumtext DEFAULT NULL,
  `description` mediumtext DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `image` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `brief` mediumtext DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table blog.articles: ~17 rows (approximately)
DELETE FROM `articles`;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` (`id`, `title`, `description`, `updated_at`, `created_at`, `image`, `created_by`, `brief`, `category_id`, `tag_id`) VALUES
  (8, 'Pasta e1', '<p>wertryyjhj</p>', NULL, '2020-09-02 08:34:38', '/uploads/20200902083438_img_3.jpg', 1, 'Italian foods are good', NULL, NULL),
  (9, 'Dessert', '<p>huhjkjkb</p>', NULL, '2020-09-02 08:35:01', '/uploads/20200902083501_img_7.jpg', 1, 'ddd', NULL, NULL),
  (10, 'abc', '<p>gcuhvujhjhfgbh</p>', NULL, '2020-09-02 08:35:19', '/uploads/20200902083519_img_2.jpg', 1, 'jdsds', NULL, NULL),
  (11, 'xyz', '<p>feuhjifjfid</p>', NULL, '2020-09-02 10:59:33', '/uploads/20200902105933_img_1.jpg', 1, 'jhucdecdk', NULL, NULL),
  (12, 'fdg', '<p>hxxsjk</p>', NULL, '2020-09-02 11:05:45', '/uploads/20200902110545_img_2.jpg', 1, 'shshhj', NULL, 0),
  (13, 'fdgf', '<p>cjvjkdckj</p>', NULL, '2020-09-02 11:06:12', '/uploads/20200902110612_img_2.jpg', 1, 'fyuk', NULL, 0),
  (14, 'Noodles', '<p>hdijdksk</p>', NULL, '2020-09-02 11:33:26', '/uploads/20200902113326_img_1.jpg', 1, 'jcskn', 0, 0),
  (15, 'Pizza', '<p>jhhdkkxdnxd</p>', NULL, '2020-09-02 11:56:52', '/uploads/20200902115652_img_2.jpg', 1, 'gssiisk', 2, 3),
  (16, 'Pizza15', '<p>hxsxxjsks</p>', NULL, '2020-09-02 11:58:13', '/uploads/20200902115813_img_1.jpg', 1, 'hrhfj', 1, 2),
  (17, 'title 1', '<p>jbkhvjv</p>', NULL, '2020-09-03 10:29:50', '/uploads/20200903102950_rmt_logo_300X200.png', 2, 'brief 1', 1, NULL),
  (18, 'title 1', '<p>jbkhvjv</p>', NULL, '2020-09-03 10:33:45', '/uploads/20200903103345_rmt_logo_300X200.png', 2, 'brief 1', 1, NULL),
  (19, 'title 1', '<p>jbkhvjv</p>', NULL, '2020-09-03 10:34:25', '/uploads/20200903103425_rmt_logo_300X200.png', 2, 'brief 1', 1, NULL),
  (20, 'title 1', '<p>jbkhvjv</p>', NULL, '2020-09-03 10:35:54', '/uploads/20200903103554_rmt_logo_300X200.png', 2, 'brief 1', 1, NULL),
  (21, 't1', '<p>ghj</p>', NULL, '2020-09-04 10:48:12', '/uploads/20200904104812_rmt_logo_300X200.png', 2, 't1 br', 2, NULL),
  (22, 't1', '<p>ghj</p>', NULL, '2020-09-04 10:48:33', '/uploads/20200904104833_rmt_logo_300X200.png', 2, 't1 br', 2, NULL),
  (23, NULL, NULL, NULL, '2020-09-04 10:55:15', NULL, 2, NULL, 1, NULL),
  (24, NULL, NULL, NULL, '2020-09-04 10:55:58', NULL, 2, NULL, 1, NULL);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- Dumping structure for table blog.article_tags
DROP TABLE IF EXISTS `article_tags`;
CREATE TABLE IF NOT EXISTS `article_tags` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table blog.article_tags: ~4 rows (approximately)
DELETE FROM `article_tags`;
/*!40000 ALTER TABLE `article_tags` DISABLE KEYS */;
INSERT INTO `article_tags` (`at_id`, `article_id`, `tag_id`, `updated_at`, `created_at`, `created_by`) VALUES
  (1, 20, 2, NULL, '2020-09-03 10:35:55', 2),
  (2, 20, 6, NULL, '2020-09-03 10:35:55', 2),
  (3, 21, 2, NULL, '2020-09-04 10:48:12', 2),
  (4, 22, 2, NULL, '2020-09-04 10:48:33', 2);
/*!40000 ALTER TABLE `article_tags` ENABLE KEYS */;

-- Dumping structure for table blog.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`cat_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table blog.categories: ~3 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`cat_id`, `category_name`, `status`) VALUES
  (1, 'Indian', 1),
  (2, 'Italian', 0),
  (3, 'Chinese', 1);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table blog.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table blog.migrations: ~0 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table blog.posts
DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL DEFAULT '0',
  `description` text NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `image` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table blog.posts: ~0 rows (approximately)
DELETE FROM `posts`;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for table blog.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Dumping data for table blog.roles: ~4 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role_name`) VALUES
  (7, 'Admin'),
  (8, 'Author'),
  (9, 'Viewer'),
  (10, 'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table blog.tags
DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` text NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table blog.tags: ~11 rows (approximately)
DELETE FROM `tags`;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`tag_id`, `tag_name`) VALUES
  (1, '#hyu'),
  (2, 'hi'),
  (3, 'xyz123456'),
  (4, 'somenewtag'),
  (7, 'sad'),
  (8, 'sad'),
  (9, 'shas'),
  (10, 'shalini'),
  (11, 'something'),
  (12, '123456'),
  (13, 'sad');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table blog.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table blog.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
  (1, 'bhavana', 'bhavana@gmail.com', NULL, '$2y$10$OjwFvRO0DnLrpv72dU/od./llDH.aHwNMJSVtSzLwkgtuwBC7huoG', NULL, '2020-08-26 14:58:36', '2020-08-26 14:58:36'),
  (2, 'Shashank', 'shashank@shashank.com', NULL, '$2y$10$OjwFvRO0DnLrpv72dU/od./llDH.aHwNMJSVtSzLwkgtuwBC7huoG', NULL, '2020-09-03 10:21:48', '2020-09-03 10:21:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table blog.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `ur_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`ur_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Dumping data for table blog.user_roles: ~3 rows (approximately)
DELETE FROM `user_roles`;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`ur_id`, `user_id`, `role_id`, `updated_at`, `created_at`, `created_by`) VALUES
  (1, 1, 7, '2020-09-03 16:14:29', '2020-09-03 16:14:30', 1),
  (2, 2, 8, '2020-09-03 16:14:43', '2020-09-03 16:14:44', 1),
  (3, 3, 7, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
