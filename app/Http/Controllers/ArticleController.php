<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Session;
use App\Article;
use App\Tag;

class ArticleController extends Controller
{
  //
  public function welcome(){
    // dd(public_path());
    $data = [];
    $posts = DB::table('articles as a')->select(
      'a.id as ArticleID','u.id as UserID','a.*','u.*'
      )->leftjoin('users as u','u.id','=','a.created_by')->get();
      // dd($posts);
      $data['slider_posts'] = $posts->take(3);
      $data['recent_posts'] = $posts;

      return view('welcome',$data);
    }
    public function all_articles()
    {
      $data = [];
      // $posts = Article::get();
      $posts = DB::table('articles')->get();
      // dd($posts);
      $data['all_posts'] = $posts;
      $data['students'] = ['content','deeksha','ajith','bhavna'];
      // dd($data['students']);
      return view('all_articles',$data);
    }

    public function read_article()
    {
      echo 'This is page where your single article description comes up';
    }

    public function insert_article()
    {
      $data = [];
      $tags = Tag::GetAllTags();
      $categories=DB::table('categories')->get();
      $data['tags'] = $tags;
      $data['categories']=$categories;
      return view('insert_article',$data);
    }

    public function article_save(Request $r)
    {

      // dd($r->all());
      try {

        $title = $r['title'];
        $description = $r['description'];
        $brief = $r['brief'];

        $article_tags = $r['tag_name'];

        $file = $r['image'];
        $short_url=UploadController::uploader($file);

        $insert_data = [
          'title' => $title,
          'description' => $description,
          'created_at' => date('Y-m-d H:i:s'),
          'image' => $short_url,
          'brief'=> $r['brief'],
          'created_by' => Auth::user()->id,
          // 'tag_id'=>$r['tag_name'],
          'category_id'=>$r['category_name']
        ];
        $article_id = DB::table('articles')->insertGetID($insert_data);
        $insert_data = [];
        foreach($article_tags as $key => $t) {
          if (is_numeric($t)) {
            $tag_id = $t;
          } else {
            $tag_id = DB::table('tags')->insertGetID([
              'tag_name' => $t
            ]);
          }
          array_push($insert_data,[
            'article_id'=> $article_id,
            'tag_id'=> $tag_id,
            'created_by' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
          ]);
        }
        DB::table('article_tags')->insert($insert_data);

        Session::flash('msg','Post saved');
        Session::flash('msg_class','success');

      } catch (\Exception $e) {

        Session::flash('msg','Post not saved');
        dd($e);
        Session::flash('msg_class','danger');

      }



      return back();

    }
    public function contact_us(Request $r)
    {
      return view('contact_us');
      // echo 'This is page where your single article description comes up';
    }
    public function single_post(Request $r){

      // dd($r->all());
      $data = [];
      $id = $r['id'];

      $post = DB::table('articles')->where('id',$id)->first();
      $data['post_data'] = $post;
      return view('single_post',$data);
    }
    public function about_us(Request $r)
    {
      return view('about_us');
    }
  }
