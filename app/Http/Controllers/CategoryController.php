<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CategoryController extends Controller
{
    //
    public function categories()
    {
        $data = [];   
        $categories = DB::table('categories')->get();
        $data['categories'] = $categories;
        return view('categories',$data);
    }
    public function insert_category()
    {
        //dd($r->all());
        return view('insert_category');
    }
    public function save_category(Request $r)
    {
        //dd($r->all());
        $insertData = [
          'category_name' => $r['category_name'],
          'status' => $r['status']
        ];
        DB::table('categories')->insert($insertData);

        return redirect()->route('category');
    }
}
