<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



use Auth;
use DB;
use Session;

use App\Post;



class PostController extends Controller
{
  public function index(){
    // dd(public_path());
    $data = [];
    $posts = DB::table('articles')->get();
    //dd($posts);
    $data['recent_posts'] = $posts;

    return view('post.index',$data);
  }
  public function form()
  {
    // dd(\Session::all());
    $this->data = [];
    // $this->data['name'] = 'ajith';
    return view('post.form',$this->data);
  }

  public function save(Request $r)
  {
    // dd($r->all());
    // save operation cades;

    // $date = date('Y-m-d H:i:s');

    // dd($date);
    try {

      $insertData = [
        'title' => $r['post_title'],
        'description' => $r['post_description'],
        'created_at' => date('Y-m-d H:i:s')
      ];

      DB::table('posts')->insert($insertData);

      Session::put('msg_exists',true);
      Session::put('msg_class','success');
      Session::put('msg','Saved successfully');

    } catch (\Exception $e) {
      // dd($e->getMessage());
      Session::put('msg_exists',true);
      Session::put('msg_class','danger');
      Session::put('msg','Failed to save !!!'.$e->getMessage());
    }

    return redirect()->route('admin-post-index');
  }

  public function edit_save(Request $r)
  {
    $post_id=$r['id'];
    $new_title= $r['title'];
    $new_brief=$r['brief'];
    $new_description= $r['description'];

    $old_file = $r['old_cover_pic'];
    $file = $r['image'];
    $short_url = null;
    if ($file) {
      $name =  pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
      $ext = $file->getClientOriginalExtension();
      $filename = date('YmdHis').'_'.$name.'.'.$ext;
      $path = '/uploads/';
      $targetDir = public_path().$path;
      if($file->move($targetDir,$filename)){
        $short_url = $path.$filename;
      }
    } else {
      $short_url = $old_file;
    }

    $post_data = [
      'image' => $short_url,
      'title' => $new_title,
      'brief' => $new_brief,
      'description' => $new_description
    ];

    DB::table('articles')
    ->where('id', $post_id)
    ->update($post_data);

    return redirect()->route('welcome');
  }

  public function edit(Request $r){


    $data = [];
    $id = $r['id'];


    $post = DB::table('articles')->where('id',$id)->first();
    $data['post_data'] = $post;
    // dd($post);
    return view('post.editform',$data);

  }


}
