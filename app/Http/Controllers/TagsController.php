<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Tag;
class TagsController extends Controller
{
   //
   public function tags()
   {
       $data = [];
       $tags = Tag::GetAllTags();
       $data['tags'] = $tags;
       return view('tags',$data);
   }
   public function insert_tag()
   {
       //dd($r->all());
       return view('insert_tag');
   }
   public function save_tag(Request $r)
   {
       //dd($r->all());
       $insertData = [
         'tag_name' => $r['tag_name']
       ];
       DB::table('tags')->insert($insertData);
       \Session::flash('msg','Tag saved');

       return redirect()->route('tag');
   }


   public function editform(Request $r)
   {
     $tag_id = $r['id'];

     $this->data['tag'] = Tag::where('tag_id',$tag_id)->first();

     // dd($tag);

     return view('tag_editform',$this->data);
   }

   public function editsave(Request $r)
   {
     // dd($r->all());
     $tag_id = $r['tag_id'];
     $new_tag_name = $r['tag_name'];

     $affected = DB::table('tags')
              ->where('tag_id', $tag_id)
              ->update(['tag_name' => $new_tag_name]);

      Session::flash('msg','Tag updated');
      Session::flash('msg_class','success');

      return redirect()->route('tag');
   }



}
