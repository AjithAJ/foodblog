<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Tag;
class UploadController extends Controller
{
    static public function upload(Request $r)
    {
        // dd($r['upload']);
        $file = $r['upload'];
       $url=self::uploader($file);  
    //    dd($url);
    // return asset($url);
    $CKEditorFuncNum = $r->input('CKEditorFuncNum');
      // $url = asset('storage/uploads/'.$filenametostore);
      $url = asset($url);
      $msg = 'Image successfully uploaded';
      $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

      // return response()->json(['data' => $re]);
      // Render HTML output
      @header('Content-type: text/html; charset=utf-8');
      echo $re;

  
    }
    static public function uploader($file)
    {
        
        $short_url = null;
        if ($file) {
            $name =  pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $ext = $file->getClientOriginalExtension();
            $filename = date('YmdHis').'_'.$name.'.'.$ext;
            $path = '/uploads/';
            $targetDir = public_path().$path;
            if($file->move($targetDir,$filename)){
              $short_url = $path.$filename;
            }
          } 
          return $short_url;
    }
}