<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $user = Auth::user();

      $is_admin = DB::table('user_roles')->where('user_id',$user->id)->where('role_id','7')->get();


      if (count($is_admin) > 0) {
        return $next($request);
      } else {
        return redirect()->route('access_403');
      }

    }
}
