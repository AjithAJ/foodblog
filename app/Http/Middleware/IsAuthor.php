<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class IsAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $user = Auth::user();

      $is_author = DB::table('user_roles')->where('user_id',$user->id)->where('role_id','8')->get();


      if (count($is_author) > 0) {
        dd($request);
        return $next($request);
      } else {
        return redirect()->route('access_403');
      }
    }
}
