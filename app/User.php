<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Auth;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    static public function user_roles()
    {
      if (Auth::check()) {
        $results = DB::table('user_roles as ur')
        ->leftjoin('roles as r','r.role_id','=','ur.role_id')
        ->where('user_id',Auth::user()->id)
        // ->select('r.role_name','ur.role_id','ur.user_id')
        // ->get();
        ->pluck('ur.role_id')
        ->toArray();
      } else {
        $results = [];
      }
      return $results;
    }
}
