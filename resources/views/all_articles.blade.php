@extends('layouts.mainlayout')
    @section('content')
    <div class="overlay"></div>
    <div class="container">
      <h3>ALL POSTS</h3>
     </div>
     <div class="row">
       @foreach($all_posts as $key => $post)
       <div class="container">
    <div class="row">
        <div class="col-12 mt-3">
            <a href="{{route('single_post')}}"><div class="card" id="horizontal">
                <div class="card-horizontal">
                    <div class="img-square-wrapper">
                        <img class="all-img" src="{{asset($post->image)}}" alt="Card image cap">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">{{$post->title}}</h4>
                        <p class="card-text">{{$post->brief}}</p>
                    </div>
                </div>
                <div class="card-footer">
                    <small class="text-muted"></small>
                </div>
            </div></a>
        </div>
    </div>
</div>
      
        @endforeach
    </div>
</div>
    @endsection