@extends('layouts.mainlayout')
@section('content')
<div class="container">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">sl.no</th>
      <th scope="col">Category</th>
      <th scope="col">status</th>
    </tr>
  </thead>
  <tbody>
  @foreach($categories as $key => $category)
    <tr>
      <th scope="row">{{$key+1}}</th>
      <td>{{$category->category_name}}</td>
      <td>{{$category->status == 0? "Inactive":"Active"}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
  </tbody>
</table>
</div>
@endsection