@extends('layouts.mainlayout')



@section('content')
<div class="container">
    <h1>Insert Article</h1>
    <form action="{{route('article_save')}}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="row">
        <div class="col-md-12">

                <div class="form-group">

                    <label for="">Title</label>
                    <input type="text" class="form-control" name="title">
                </div>
            </div>
            <div class="col-md-12">

                <div class="form-group">

                    <label for="">Brief</label>
                    <input type="text" class="form-control" name="brief">
                </div>
            </div>
            <div class="col-md-12">
            <div class="form-group">

                    <label for="image">Upload an Image</label>
                    <input type="file" class="form-control-file" name="image" id="image" accept="image/*">


                </div>
        </div>
                <div class="col-md-12 mb-3">
                <div class="form-group">
                   <label for="">Category</label>
                   <div class="d-flex">
                  <select class="form-control" name="category_name">
                   @foreach($categories as $key=>$category)
                   <option value="{{$category->cat_id}}">{{$category->category_name}}</option>
                   @endforeach
                   </select>
                       <div class="col-md-6 mb-3">
                       <a class="btn bt-lg" href="{{route('insert_category')}}" role="button"><i class="fas fa-plus-circle"></i> Add a new category</a>
                       </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Content</label>
                    <textarea rows="10" width="100%" class="form-control" name="description"></textarea>
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <div class="form-group">
                   <label for="">Tags</label>
                   <div class="d-flex">
                   <select class="form-control makeselect2 mr-1" name="tag_name[]" multiple="multiple">
                   @foreach($tags as $key=>$tag)
                   <option value="{{$tag->tag_id}}">{{$tag->tag_name}}</option>
                   @endforeach
                </select>
                       <div class="col-md-6 mb-3">
                       <a class="btn bt-lg" href="{{route('insert_tag')}}" role="button"><i class="fas fa-plus-circle"></i> Add a new tag</a>
                       </div>
                </div>
            </div>

            </div>

        </div>
        <button type="submit" class="btn "><i class="fas fa-paper-plane"></i>  Publish</button>
    </form>

</div>
@endsection


@section('afterScripts')
<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
    $('.makeselect2').select2({
      'tags': true
    });

                        CKEDITOR.replace( 'description' );
                </script>
@endsection
