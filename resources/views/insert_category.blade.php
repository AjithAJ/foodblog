@extends('layouts.mainlayout')
@section('content')
<div class="container">
<form action="{{route('save_category')}}" method="post" enctype="multipart/form-data">
@csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Add a Category</label>
    <input type="text" class="form-control"  placeholder="Enter category" name="category_name">
  </div>
  <div class="form-check form-check-inline">
  <input class="form-check-input" type="radio"  id="inlineRadio1" value="1" name="status">
  <label class="form-check-label" for="inlineRadio1">Active</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="radio"  id="inlineRadio2" value="0" name="status">
  <label class="form-check-label" for="inlineRadio2">InActive</label>
</div>
<div>
  <button type="submit" class="btn ">Add category</button>
  </div>
</form>
</div>
@endsection