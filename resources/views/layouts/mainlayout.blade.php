<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
 <link href="{{ asset('css/style.css') }}"  rel="stylesheet">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="{{asset('/simplyToast/simplyToast.css')}}">

 <script src="https://kit.fontawesome.com/2d0bbcfa70.js" crossorigin="anonymous"></script>
 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <title>Food Blog</title>
</head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light m-nbar">
  <a class="navbar-brand" href="/">Food Blog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('all_articles')}}">All Post</a>
      </li>
      <li class="nav-item">
        <a class="nav-link " href="{{route('contact_us')}}">Contact Us</a>
      </li>

      @if(in_array('7',\App\User::user_roles()))
      <li class="nav-item">
        <div class="dropdown">
          <a class="nav-link  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Categories
          </a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('category')}}">All categories</a>
            <a class="dropdown-item" href=" {{route('insert_category')}}">Add a category</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <div class="dropdown">
          <a class="nav-link  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Tags  </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="{{route('tag')}}">All tags</a>
              <a class="dropdown-item" href=" {{route('insert_tag')}}">Add a tag</a>
            </div>
          </div>
        </li>
        @endif
    </ul>
    <ul class="navbar-nav ml-auto">
      <!-- <li class="nav-item active">
        <a class="nav-link" href="{{route('login')}}"> Login </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('register')}}">Register</a>
      </li> -->
      @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('admin-post-index') }}">Edit Post</a>
                            </li>
                            <li class="nav-item">
        <a class="nav-link" href="{{route('insert_article')}}">Post Article</a>
      </li>
      <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                     <span class="fas fa-user"></span> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
    </ul>
    <!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->
  </div>
</nav>

<div class="maincontent" style="margin-top: 57px;">


   @yield('content')
        </div>
    <!-- <h1>Hello, world!</h1> -->

 <footer class="site-footer">
<div class="container">
<div class="row mb-5">
<div class="col-md-4">
<h3>About Us</h3>
<p class="mb-4">
<img src="{{asset('/images/img_1.jpg')}}" alt="Image placeholder" class="img-fluid">
</p>
<p>Lorem ipsum dolor sit amet sa ksal sk sa, consectetur adipisicing elit. Ipsa harum inventore reiciendis. <a href="{{route('about_us')}}">Read More</a></p>
</div>
<div class="col-md-6 ml-auto">
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-8">
<div class="mb-5">
<h3>Quick Links</h3>
</br>
<ul class="list-unstyled">
<li><a href="#">Scroll Back to top</a></li></br>
<li><a href="{{route('all_articles')}}">All Posts</a></li></br>
<li><a href="{{route('insert_article')}}">Post Article</a></li></br>
<li><a href="{{route('contact_us')}}">Contact Us</a></li></br>

</ul>
</div>
</div>
<div class="mb-5">
<h3>Social</h3>
</br>
<ul class="list-unstyled footer-social">
<li><a href="#"><span class="fa fa-twitter"></span> Twitter</a></li>
<li><a href="#"><span class="fa fa-facebook"></span> Facebook</a></li>
<li><a href="#"><span class="fa fa-instagram"></span> Instagram</a></li>
<li><a href="#"><span class="fa fa-youtube-play"></span> Youtube</a></li>
<li><a href="#"> <span class="fa fa-snapchat"></span> Snapchat</a></li>



</ul>
</div>
</div>
</div>
</div>
</div>
</footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{asset('/simplyToast/simplyToast.js')}}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});</script>
    @yield('afterScripts')

    <script>
    @if(Session::has('msg'))
    $.simplyToast("{{Session::get('msg')}}","{{Session::get('msg_class')}}");
    @endif
    </script>
  </body>
</html>
