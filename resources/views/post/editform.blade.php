@extends('layouts.mainlayout')






@section('content')
<div class="container">
  <h1>Edit Article</h1>
  <form action="{{route('admin-post-edit-save')}}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$post_data->id}}">
    <div class="row">
      <div class="col-md-12">

        <div class="form-group">

          <label for="">Title</label>
          <input type="text"   class="form-control" name="title" value="{{$post_data->title}}">
        </div>
      </div>
      <div class="col-md-12">

        <div class="form-group">



          <label for="">Brief</label>

          <input type="text"  class="form-control" name="brief" value="{{$post_data->brief}}">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">

          <label for="image">Upload an Image</label>
          <input type="file" class="form-control" name="image" id="image" accept="image/*" >
          <input type="hidden" name="old_cover_pic" value="{{$post_data->image}}">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="">Content</label>
          <textarea rows="10" width="100%" class="form-control" name="description" >{!!$post_data->description!!}</textarea>
        </div>
      </div>
    </div>
    <a href="{{route('admin-post-edit-save')}}">
      <button type="submit" class="btn ">Save</button></a>
    </form>

  </div>

  @endsection
@section('afterScripts')
<script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
                        CKEDITOR.replace( 'description', {
	// filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
	filebrowserUploadUrl: '/ckeditor/fileupolader?_token={{csrf_token()}}',
  filebrowserUploadMethod: 'post'
});
                </script>
@endsection