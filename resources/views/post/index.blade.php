@extends('layouts.mainlayout')


@section('content')
    </style>
 <div class="container">
        <div class="row mb-5 mt-5 ">
          <div class="col-12">
            <h1>All Posts</h1>
          </div>
        </div>
<div class="container-fluid">
<div class="row">
 @foreach($recent_posts as $key => $post)
<div class="col-md-4 mt-4">
<div class="card h-100" style="width: 18rem;">
  <img class="card-img-top" src="{{asset($post->image)}}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">{{$post->title}}</h5>
                <span class="d-inline-block mt-1"><span class="fa fa-user-edit"></span><a href="#"> Ajith</a></span>
                <span>&nbsp;-&nbsp; August 25, 2020</span>
    <p class="card-text">{{$post->brief}}</p>
    <a href="{{route('single_post',['id'=>$post->id])}}" class="btn mr-5">Read it!</a> 
    <a href="{{route('admin-post-edit',['id'=>$post->id] )}}" class="btn ml-4"><i class="far fa-edit"></i>  Edit</a> 
  </div>
</div>
</div>
 @endforeach
</div>
</div>
@endsection
