@extends('layouts.mainlayout')
    

    @section('content')
    <div class="container">
        <div class="col-lg-10">
            <div class="single-title">
            {{$post_data->title}}
            </div>
           <img src= "{{asset($post_data->image)}}" alt="Image" class="img-fluid mb-5">
           <div class="single-brief">
                 <h5>{{$post_data->brief}}</h5>
           </div>
           <div class="single-description">
                 {!!$post_data->description!!}
           </div>
        </div>
        
    </div>


    @endsection