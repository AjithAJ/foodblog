@extends('layouts.mainlayout')
@section('content')

<div class="container">
<form action="{{route('tag-editsave')}}" method="post" enctype="multipart/form-data">
@csrf
<input type="hidden" name="tag_id" value="{{$tag->tag_id}}">
 <div class="form-group">
   <label for="exampleInputEmail1">Edit tag name</label>
   <input type="text" class="form-control"  placeholder="Enter tag" name="tag_name" value="{{$tag->tag_name}}">
 </div>

 <button type="submit" class="btn ">Update</button>
 </div>
</form>
</div>
@endsection
