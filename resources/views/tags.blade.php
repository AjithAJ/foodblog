@extends('layouts.mainlayout')
@section('content')


<div class="container">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">sl.no</th>
      <th scope="col">Tag Name</th>
    </tr>
  </thead>
  <tbody>
  @foreach($tags as $key => $tag)
    <tr>
      <th scope="row">{{$key+1}}</th>
      <td>{{$tag->tag_name}}</td>
      <td>
        <a href="{{route('tag-editform',['id'=>$tag->tag_id])}}"> <i class="fa fa-pencil"></i> </a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
  </tbody>
</table>
</div>
@endsection
