@extends('layouts.mainlayout')
    @section('content')


<div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          @foreach($slider_posts as $key => $post)
      <li data-target="#carouselExampleCaptions" data-slide-to="{{$key}}" class="{{ $key == '0' ? 'active' : '' }}"></li>
      @endforeach
    </ol>
    <div class="carousel-inner">
      @foreach($slider_posts as $key => $post)
      <div class="carousel-item  {{ $key == '0' ? 'active' : '' }}">
        <img src="{{asset($post->image)}}" class="d-block w-100 testimonials" alt="...">
        <div class="carousel-caption d-none d-md-block ">
          <h5 class= "text-white bg-dark">{{$post->title}}</h5>
          <p>{{$post->brief}}</p>
        </div>
      </div>
      @endforeach
      <!-- <div class="carousel-item">
        <img src="images/img_3.jpg" class="d-block w-100 testimonials" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Second slide label</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="images/img_2.jpg" class="d-block w-100 " alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Third slide label</h5>
          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
        </div>
      </div>
    </div> -->
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

    </div>
  </div>
  </div>

 <div class="container">
        <div class="row mb-5 mt-5 ">
          <div class="col-12">
            <h1>Recent Posts</h1>
          </div>
        </div>
<div class="container">
<div class="row">
 @foreach($recent_posts as $key => $post)
<div class="col-md-4 mt-4">
<div class="card h-100" style="width: 18rem;">
<div class="card-header">
    Featured
  </div>
  <img class="card-img-top card-img" src="{{asset($post->image)}}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">{{$post->title}}</h5>
                <span class="d-inline-block mt-1"><span class="fa fa-user-edit" class="icon" ></span><a href="#"> {{$post->name ? $post->name : 'Unknown'}}</a></span>
                <span>&nbsp;-&nbsp; {{$post->created_at}}</span>
    <p class="card-text">{{$post->brief}}</p>
    <div class="button">
    <a href="{{route('single_post',['id'=>$post->ArticleID])}}" class="btn">Read it!</a>
</div>
  </div>
</div>
</div>
 @endforeach

</div>
</div>
 @endsection
