<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ArticleController@welcome')->name('welcome');

Route::post('/ckeditor/fileupolader','UploadController@upload');
Route::get('/article/all','ArticleController@all_articles')->name('all_articles');
Route::get('/article/read','ArticleController@read_article')->name('read_article');

Route::get('/admin/article/insert','ArticleController@insert_article')->name('insert_article');
Route::post('/admin/article/save','ArticleController@article_save')->name('article_save');

Route::middleware(['IsAdmin'])->group(function () {

    Route::get('/category/categories','CategoryController@categories')->name('category');
    Route::get('/category/addcategory','CategoryController@insert_category')->name('insert_category');
    Route::post('/category/savecategory','CategoryController@save_category')->name('save_category');

    Route::get('/tag/tags','TagsController@tags')->name('tag');
    Route::get('/tag/addtags','TagsController@insert_tag')->name('insert_tag');
    Route::post('/tag/savetags','TagsController@save_tag')->name('save_tag');

    Route::get('/tag/edit','TagsController@editform')->name('tag-editform');
    Route::post('/tag/editsave','TagsController@editsave')->name('tag-editsave');

});

Route::get('/admin/about','ArticleController@about_us')->name('about_us');
Route::get('/admin/posts','PostController@index')->name('admin-post-index');
Route::get('/admin/post/form','PostController@form')->name('admin-post-form');
Route::post('/admin/post/save','PostController@save')->name('admin-post-save');
Route::get('/admin/post/edit','PostController@edit')->name('admin-post-edit');
Route::post('/admin/post/edit/save','PostController@edit_save')->name('admin-post-edit-save');
Route::get('/admin/contact','ArticleController@contact_us')->name('contact_us');
Route::post('/admin/contact/save','ArticleController@contact_save')->name('contact_save');
Route::get('/admin/single','ArticleController@single_post')->name('single_post');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/access/403', 'HomeController@access_403')->name('access_403');
